#include "Radio.h"
#include "Channel.h"

RF24 receiver(7,  8);
ChannelData rx_data;

uint8_t address[][6] = {"RCSYS"};

namespace
{
unsigned long last_rx_time = 0;
}

void radio_loop()
{
  while (receiver.available())
  {
    //Serial.println("receiving");

    receiver.read(&rx_data, sizeof(rx_data));
    last_rx_time = millis();
    
    rx_to_channel();
  }

  radio_test_timeout();
}

bool radio_test_timeout()
{
  if (millis() - last_rx_time > RADIO_RX_INTERVAL)
  {
    //Serial.println("We are here (timeout)");
    
    radio_reset();

    return false;
  }

  return true;
}

void radio_reset()
{
  rx_data.ch1 = 1500;
  rx_data.ch2 = 1500;
  rx_data.ch3 = 1500;
  rx_data.ch4 = 1500;
  rx_data.ch5 = 1500;
  rx_data.ch6 = 1500;
  rx_data.ch7 = 1500;
  rx_data.ch8 = 1500;
  rx_data.ch9 = 1500;
  rx_data.ch10 = 1500;
  rx_data.ch11 = 1500;
  rx_data.ch12 = 1500;

  rx_to_channel();
}

bool radio_setup()
{
  // while(!receiver.begin());

  if(receiver.begin())
  {
    #ifdef DEBUG
    Serial.println("Radio connected");
    #endif
  }
  else
  {
    #ifdef DEBUG
    Serial.println("Radio disconnected");
    #endif    

    return false;
  }
  
  radio_reset();
  
  receiver.setAutoAck(false);
  receiver.setRetries(0, 0);
  receiver.setPALevel(RF24_PA_MAX);
  receiver.setDataRate(RF24_250KBPS);
  receiver.setPayloadSize(sizeof(rx_data));
  receiver.startListening();
  receiver.openReadingPipe(1, address[0]);

  return true;
}
