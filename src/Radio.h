#ifndef RADIO_H
#define RADIO_H

#include <Arduino.h>
#include <RF24.h>
#include "Data.h"
#include "Debug.h"

#define RADIO_RX_INTERVAL 250

extern RF24 receiver;
extern ChannelData rx_data;

bool radio_setup();
void radio_loop();
bool radio_test_timeout();
void radio_reset();

#endif