#ifndef CHANNEL_H
#define CHANNEL_H

#include <Servo.h>
#include "Debug.h"

using Channel = Servo;

extern Channel ch1, ch2, ch3, ch4, ch5, ch6,
  ch7, ch8, ch9, ch10, ch11, ch12;

void setup_channels();
void rx_to_channel();

#endif
