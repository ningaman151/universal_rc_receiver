#include "Channel.h"
#include "Pins.h"
#include "Radio.h"

Channel ch1, ch2, ch3, ch4, ch5, ch6,
  ch7, ch8, ch9, ch10, ch11, ch12;
  
void setup_channels()
{
  ch1.attach(CH1_PIN);
  ch2.attach(CH2_PIN);
  ch3.attach(CH3_PIN);
  ch4.attach(CH4_PIN);
  ch5.attach(CH5_PIN);
  ch6.attach(CH6_PIN);
  ch7.attach(CH7_PIN);
  ch8.attach(CH8_PIN);
  ch9.attach(CH9_PIN);
  ch10.attach(CH10_PIN);
  ch11.attach(CH11_PIN);
  ch12.attach(CH12_PIN);
}

void rx_to_channel()
{
  ch1.writeMicroseconds(rx_data.ch1);
  ch2.writeMicroseconds(rx_data.ch2);
  ch3.writeMicroseconds(rx_data.ch3);
  ch4.writeMicroseconds(rx_data.ch4);
  ch5.writeMicroseconds(rx_data.ch5);
  ch6.writeMicroseconds(rx_data.ch6);
  ch7.writeMicroseconds(rx_data.ch7);
  ch8.writeMicroseconds(rx_data.ch8);
  ch9.writeMicroseconds(rx_data.ch9);
  ch10.writeMicroseconds(rx_data.ch10);
  ch11.writeMicroseconds(rx_data.ch11);
  ch12.writeMicroseconds(rx_data.ch12);
}
