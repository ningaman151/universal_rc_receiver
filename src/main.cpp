#include <Arduino.h>
#include "Radio.h"
#include "Channel.h"
#include "Debug.h"

void setup()
{
  setup_channels();
  radio_setup();

#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("Starting");
#endif
}

void loop()
{
  radio_loop();

#ifdef DEBUG
  Serial.print(rx_data.ch1);
  Serial.print(" ");
  Serial.print(rx_data.ch2);
  Serial.print(" ");
  Serial.print(rx_data.ch3);
  Serial.print(" ");
  Serial.print(rx_data.ch4);
  Serial.print(" ");
  Serial.print(rx_data.ch5);
  Serial.print(" ");
  Serial.print(rx_data.ch6);
  Serial.print(" ");
  Serial.print(rx_data.ch7);
  Serial.print(" ");
  Serial.print(rx_data.ch8);
  Serial.print(" ");
  Serial.print(rx_data.ch9);
  Serial.print(" ");
  Serial.print(rx_data.ch10);
  Serial.print(" ");
  Serial.print(rx_data.ch11);
  Serial.print(" ");
  Serial.println(rx_data.ch12);
#endif
}