#ifndef PINS_H
#define PINS_H

#include "Debug.h"

//Proto board
// #define CH1_PIN 4
// #define CH2_PIN 3
// #define CH3_PIN 2
// #define CH4_PIN A5
// #define CH5_PIN A4
// #define CH6_PIN A3
// #define CH7_PIN A2
// #define CH8_PIN A1
// #define CH9_PIN A0
// #define CH10_PIN 6
// #define CH11_PIN 5

//PCB
#define CH1_PIN A5
#define CH2_PIN A4
#define CH3_PIN A3
#define CH4_PIN A2
#define CH5_PIN A1
#define CH6_PIN A0
#define CH7_PIN 10
#define CH8_PIN 9
#define CH9_PIN 6
#define CH10_PIN 5
#define CH11_PIN 4
#define CH12_PIN 3
// #define CH13_PIN 2
// #define CH14_PIN 1
// #define CH15_PIN 0

#endif
